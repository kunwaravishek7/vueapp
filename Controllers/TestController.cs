﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TestServices.Models;
using VueApp.Database.Table;
using VueApp.Repository;

namespace VueApp.Controllers
{
    [Produces("application/json")]
    [Route("api/Test")]

    
    public class TestController : Controller
    {
        protected IPersonRepository _personRepository;
        public TestController(IPersonRepository personRepository)
        {
            _personRepository = personRepository;
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> PersonList()
        {
            var list =await _personRepository.GetPersonList();
            return Ok(list);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> TestCreatePerson([FromBody]PersonViewModel model)
        {
            if (model != null)
            {
              await  _personRepository.Create(model);
            }
            return Ok();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> TestUpdatePerson([FromBody]PersonViewModel model)
        {
            if (model != null)
            {
                await _personRepository.Update(model);
            }
            return Ok();
        }

        [HttpGet("GetPerson/{PersonID}")]
        public IActionResult GetPersonByID([FromRoute] int PersonID)
        {
         return Ok(_personRepository.GetPersonByID(PersonID));
        }

        [HttpDelete("DeletePerson/{PersonID}")]
        public async Task<IActionResult> DeletePerson([FromRoute]int PersonID)
        {
            await _personRepository.Delete(PersonID);
            return Ok();
        }
    }
}