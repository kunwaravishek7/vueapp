﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestServices.Models
{
   public class PersonViewModel
    {
        public int PersonID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
    }
}
