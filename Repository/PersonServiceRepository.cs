﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestServices.Models;
using VueApp.Database.Table;

namespace VueApp.Repository
{
    public class PersonServiceRepository : IPersonRepository
    {
        protected  DataContext _dbContext;

        public PersonServiceRepository(DataContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task Create(PersonViewModel model)
        {
            var modelToAdd= new Person()
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Address = model.Address
            };
             _dbContext.Persons.Add(modelToAdd);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(int PersonID)
        {
            var existedData = GetPersonByID(PersonID);
            _dbContext.Remove(existedData);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<IList<PersonViewModel>> GetPersonList()
        {
           return await _dbContext.Persons
                        .Select(x => new PersonViewModel()
                         {
                            PersonID=x.PersonID,
                             FirstName = x.FirstName,
                             LastName = x.LastName,
                             Address = x.Address
                         }).OrderByDescending(x=>x.PersonID).ToListAsync();
            
         }

        public async Task Update(PersonViewModel model)
        {
            var existedData = GetPersonByID(model.PersonID);
            if (existedData != null)
            {
                existedData.FirstName = model.FirstName;
                existedData.LastName = model.LastName;
                existedData.Address = model.Address;
            }
            await _dbContext.SaveChangesAsync();
        }

        public Person GetPersonByID(int PersonID)
        {
            return _dbContext.Persons.FirstOrDefault(x => x.PersonID == PersonID);
        }
    }
    }
