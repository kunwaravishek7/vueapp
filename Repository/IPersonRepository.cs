﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestServices.Models;
using VueApp.Database.Table;

namespace VueApp.Repository
{
    public interface IPersonRepository
    {
       Task Create(PersonViewModel model);
       Task Update(PersonViewModel model);
        Task<IList<PersonViewModel>> GetPersonList();
       Task Delete(int PersonID);
        Person GetPersonByID(int PersonID);
    }
}
