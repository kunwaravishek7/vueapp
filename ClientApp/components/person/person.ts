import Vue from 'vue';
import axios from 'axios';
import { Component } from 'vue-property-decorator';

interface FetchPerson {
    firstName: string;
    lastName: string;
    address: string;
    personID: number;
}

@Component
export default class FetchPersonComponent extends Vue {
    persons: FetchPerson[] = [];
    postresult: string;
    isEdit: boolean;
    CreateOrUpdateMessage: string;

    //model
    userData: {
        personID:number,
        firstName: string,
        lastName: string,
        address: string
    };

    data() {
        return {
            postresult: this.postresult,
            userData: {},//Initiating model
            isEdit: this.isEdit,
            CreateOrUpdateMessage: ''
        }
    }
    
    mounted() {
        this.getList();
       
    }
    Create() {
       
        this.isEdit = false;
        this.CreateOrUpdateMessage = "Create Person";
    }
    getList() {
        axios.get("api/Test/PersonList").then(response => {
            this.persons = response.data
        }).catch(err => {
            console.log(err)
        })
    }

    //create
    testPostData() {
        console.log(this.userData)
        axios.post('api/Test/TestCreatePerson', this.userData)
            .then((response) => {
                this.postresult = response.data;
                this.getList();
                this.Reset();
                this.hideModal();
            }, (error) => {
                console.log(error);
            });
    }


    //delete
    Delete(PersonID: number) {
        if (confirm("Are you sure, you want to delete?")) {
            const url = "api/Test/DeletePerson/" + PersonID;
            axios.delete(url)
                .then((response) => {
                    this.getList();
                }, (error) => {
                    console.log(error);
                });
        }
        else {
            return false;
        }
    }

    //get
    testGetData(PersonID: number) {
        this.isEdit = true;
        this.CreateOrUpdateMessage = "Update Person";
        axios.get("api/Test/GetPerson/"+ PersonID)
            .then((response) => {
                this.userData = response.data;
            }, (error) => {
                console.log(error);
            });
    }

    testUpdateData() {
        axios.post('api/Test/TestUpdatePerson', this.userData)
            .then((response) => {
                this.postresult = response.data;
                this.getList();
                this.Reset();
                this.hideModal();
            }, (error) => {
                console.log(error);
            });
    }
    
    //reset
    Reset() {
        this.userData.firstName = '';
        this.userData.lastName = '';
        this.userData.address = '';
    }

    PostData() {
        if (this.isEdit) {
            this.testUpdateData();
        }
        else {
            this.testPostData();
        }
    }

    hideModal() {
        const modal = document.getElementById('exampleModal');
        if (modal === null) {
            return false;
        }
        else {
            modal.classList.remove('show');
            modal.setAttribute('aria-hidden', 'true');
            modal.setAttribute('style', 'display: none');
            const modalBackdrops = document.getElementsByClassName('modal-backdrop');

            // remove opened modal backdrop
            document.body.removeChild(modalBackdrops[0]);
        }
    }

}
